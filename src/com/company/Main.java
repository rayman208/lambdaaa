package com.company;

import java.util.Random;

public class Main {

    static void FillMas(int[] mas)
    {
        Random random = new Random();
        for (int i = 0; i < mas.length; i++)
        {
            mas[i]=random.nextInt(100);
        }
    }

    static void PrintMas(int[] mas)
    {
        for (int i = 0; i < mas.length; i++)
        {
            System.out.print(mas[i]+" ");
        }
        System.out.println();
    }

    interface IChangeable
    {
        boolean HasChange(int curElem, int nextElem);
    }

    static class RealChange implements  IChangeable
    {
        @Override
        public boolean HasChange(int curElem, int nextElem) {
            return nextElem<curElem;
        }
    }

    static boolean HasChangeBigToSmall(int curElem, int nextElem)
    {
        return nextElem>curElem;
    }

    static void SortMas(int[] mas, IChangeable func)
    {
        int temp;
        boolean sort;
        int step = 0;

        do
        {
            sort=true;

            for (int i = 0; i < mas.length-1-step; i++)
            {
                if (func.HasChange(mas[i],mas[i+1])==true)
                {
                    temp = mas[i];
                    mas[i] = mas[i + 1];
                    mas[i + 1] = temp;

                    sort = false;
                }
            }

            step++;
        }
        while (sort==false);
    }

    public static void main(String[] args)
    {
	    int[] mas = new int[10];
	    FillMas(mas);
	    PrintMas(mas);

	    //SortMas(mas,Main::HasChangeBigToSmall);
        SortMas(mas,(curElem,nextElem)->{return nextElem>curElem;});
        PrintMas(mas);

        SortMas(mas,(curElem,nextElem)->nextElem<curElem);
        SortMas(mas,new RealChange());

        SortMas(mas, new IChangeable()
        {
            @Override
            public boolean HasChange(int curElem, int nextElem) {
            return nextElem<curElem;
        }});
        PrintMas(mas);
    }
}
